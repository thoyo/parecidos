import numpy as np
import sys
import argparse



class Parecidos:

    def __init__(self, csv):
        self.data = np.fromregex(csv, r'(.{10}),(.{9}),(.+), "(.+)"', np.object)
        self.users = np.unique(self.data[:, 2])

    def msg_lens(self):
        """
        Method that writes to stdout the mean length of the user's messages
        """

        sys.stdout.write("\n\nMean length of the user's messages: \n")
        
        dict_lens = {usr:0 for usr in self.users}
        
        for user in self.users:
            data_user = self.data[self.data[:, 2] == user][:, 3]
            for idx, msg in enumerate(data_user):
                dict_lens[user] += len(msg) / (idx + 1)
        
            sys.stdout.write(user + ": %d\n" % dict_lens[user])


    def msg_bursts(self):
        """
        Method that writes to stdout the mean length of the user's bursts (consecutive messages)
        """

        sys.stdout.write("\n\nMean length of the user's bursts (consecutive messages):\n")
        
        dict_bursts = {usr:0 for usr in self.users}
        dict_idxs = {usr:0 for usr in self.users}
        last_usr = self.data[0, 2]
        tmp = 1

        for msg in self.data:
            user = msg[2]
            if user == last_usr:
                tmp += 1
            else:
                dict_bursts[last_usr] += tmp / (dict_idxs[user] + 1)
                tmp = 1
            last_usr = user
            dict_idxs[user] += 1

        for user in self.users:
            sys.stdout.write(user + ": %.5f\n" % dict_bursts[user])
     
       

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--csv', help='Whatsapp history in CSV format')
    args = parser.parse_args()

    parecidos = Parecidos(args.csv)
    parecidos.msg_lens()
    parecidos.msg_bursts()
